require 'spec_helper'
require 'faraday'
require_relative '../../app/models/name.rb'

describe 'Ranked name by UF and genre' do
  context 'female' do
    it 'GET /api/v2/censos/nomes/ranking?localidade=UF&sexo=F' do
      state_id = 35
      genre = 'F'
      json = File.read("#{PROJECT_ROOT}/spec/support/female_name_ranking_by_uf.json")
      url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}&sexo=#{genre}"
      response = double('faraday_response', body: json, status: 200)
      allow(Faraday).to receive(:get).with(url).and_return(response)
      female_ranked_list = Name.female_ranked_list(state_id)

      expect(female_ranked_list[0].name).to eq 'MARIA'
      expect(female_ranked_list[0].frequency).to eq 333333
      expect(female_ranked_list[0].ranking).to eq 1
      expect(female_ranked_list[1].name).to eq 'ANA'
      expect(female_ranked_list[1].frequency).to eq 222222
      expect(female_ranked_list[1].ranking).to eq 2
      expect(female_ranked_list[2].name).to eq 'JULIANA'
      expect(female_ranked_list[2].frequency).to eq 111111
      expect(female_ranked_list[2].ranking).to eq 3
    end

    it 'return empty JSON unless status 200' do
      json = {}
      state_id = 11
      genre = 'F'
      url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}&sexo=#{genre}"
      response = double('faraday_response', body: json, status: 500)
      allow(Faraday).to receive(:get).with(url).and_return(response)
      female_ranked_list = Name.female_ranked_list(state_id)

      expect(female_ranked_list).to be_empty
    end
  end
  
  context 'male' do
    it 'GET /api/v2/censos/nomes/ranking?localidade=UF&sexo=M' do
      state_id = 11
      genre = 'M'
      json = File.read("#{PROJECT_ROOT}/spec/support/male_name_ranking_by_uf.json")
      url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}&sexo=#{genre}"
      response = double('faraday_response', body: json, status: 200)
      allow(Faraday).to receive(:get).with(url).and_return(response)
      male_ranked_list = Name.male_ranked_list(state_id)

      expect(male_ranked_list[0].name).to eq 'JOSE'
      expect(male_ranked_list[0].frequency).to eq 322322
      expect(male_ranked_list[0].ranking).to eq 1
      expect(male_ranked_list[1].name).to eq 'JOAO'
      expect(male_ranked_list[1].frequency).to eq 211211
      expect(male_ranked_list[1].ranking).to eq 2
      expect(male_ranked_list[2].name).to eq 'CAIO'
      expect(male_ranked_list[2].frequency).to eq 100100
      expect(male_ranked_list[2].ranking).to eq 3
    end
    
    it 'return empty JSON unless status 200' do
      json = {}
      state_id = 11
      genre = 'M'
      url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}&sexo=#{genre}"
      response = double('faraday_response', body: json, status: 500)
      allow(Faraday).to receive(:get).with(url).and_return(response)
      male_ranked_list = Name.male_ranked_list(state_id)

      expect(male_ranked_list).to be_empty
    end
  end
end