require 'spec_helper'
require 'faraday'
require_relative '../../app/models/name.rb'

describe 'Name ranking by UF' do
  it 'GET /api/v2/censos/nomes/ranking?localidade=UF' do
    state_id = 35
    json = File.read("#{PROJECT_ROOT}/spec/support/name_ranking_by_uf.json")
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}"
    response = double('faraday_response', body: json, status: 200)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    general_ranked_list = Name.general_ranked_list(state_id)

    expect(general_ranked_list[0].name).to eq 'CAIO'
    expect(general_ranked_list[0].frequency).to eq 300300
    expect(general_ranked_list[0].ranking).to eq 1
    expect(general_ranked_list[1].name).to eq 'JESSICA'
    expect(general_ranked_list[1].frequency).to eq 200200
    expect(general_ranked_list[1].ranking).to eq 2
    expect(general_ranked_list[2].name).to eq 'PEDRO'
    expect(general_ranked_list[2].frequency).to eq 100100
    expect(general_ranked_list[2].ranking).to eq 3
  end

  it 'returns empty json unless status 200' do
    state_id = 22
    json = {}
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}"
    response = double('faraday_response', body: json, status: 500)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    general_ranked_list = Name.general_ranked_list(state_id)

    expect(general_ranked_list).to be_empty
  end
end