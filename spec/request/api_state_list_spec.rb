require 'spec_helper'
require 'faraday'

describe 'State list' do
  it 'should get all states infos' do
    json = File.read("#{PROJECT_ROOT}/spec/support/states_infos.json")
    url = 'http://servicodados.ibge.gov.br/api/v1/localidades/estados'
    response = double('faraday_response', body: json, status: 200)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    state_list = State.state_list

    expect(state_list.length).to eq 3
    expect(state_list[0].state_id).to eq 29
    expect(state_list[0].name).to eq 'Bahia'
    expect(state_list[0].initials).to eq 'BA'
    expect(state_list[1].name).to eq 'Rio de Janeiro'
    expect(state_list[1].initials).to eq 'RJ'
    expect(state_list[2].name).to eq 'São Paulo'
    expect(state_list[2].initials).to eq 'SP'
  end

  it 'should return empty JSON if API breaks' do
    json = {}
    url = 'http://servicodados.ibge.gov.br/api/v1/localidades/estados'
    response = double('faraday_response', body: json, status: 503)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    state_list = State.state_list

    expect(state_list).to be_empty
  end
end
