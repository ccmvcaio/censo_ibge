require 'spec_helper'
require 'faraday'
require_relative '../../app/models/name.rb'

describe Name do
  it 'general list must be formatted' do
    state_id = 35
    json = File.read("#{PROJECT_ROOT}/spec/support/name_ranking_by_uf.json")
    url = "https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}"
    response = double('faraday_response', body: json, status: 200)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    general_list = Name.general_ranked_list(state_id)
    formatted_general_list = Name.formatted_name_list(general_list)

    expect(formatted_general_list).to include('CAIO - 300300 - 1')
    expect(formatted_general_list).to include('JESSICA - 200200 - 2')
    expect(formatted_general_list).to include('PEDRO - 100100 - 3')
  end
end