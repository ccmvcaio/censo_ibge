require 'spec_helper'
require 'faraday'
require_relative '../../app/models/state.rb'

describe State do
  it 'must be formatted' do
    json = File.read("#{PROJECT_ROOT}/spec/support/states_infos.json")
    url = 'http://servicodados.ibge.gov.br/api/v1/localidades/estados'
    response = double('faraday_response', body: json, status: 200)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    formatted_state_list = State.formatted_state_list

    expect(formatted_state_list).to include('Bahia - BA')
    expect(formatted_state_list).to include('Rio de Janeiro - RJ')
    expect(formatted_state_list).to include('São Paulo - SP')
  end

  it 'select state_id by UF' do
    initials = 'SP'
    json = File.read("#{PROJECT_ROOT}/spec/support/states_infos.json")
    url = 'http://servicodados.ibge.gov.br/api/v1/localidades/estados'
    response = double('faraday_response', body: json, status: 200)
    allow(Faraday).to receive(:get).with(url).and_return(response)
    state_id = State.state_id_by_initials(initials)

    expect(state_id).to eq '35'
  end
end