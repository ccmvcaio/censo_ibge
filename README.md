# README

## CENSO IBGE

### Começando:
  Para executar esse projeto, será necessário ter instalado:
    * ruby 2.7.1

### Construção:
  Para construir o projeto você deve executar o seguinte comando no diretório escolhido:
    * bin/setup

### Features:
  O projeto possui as seguintes features:
    * Lista de estados: Onde faz uma requisição via API em http://servicodados.ibge.gov.br/api/v1/localidades/estados e retorna uma lista com id, nome e sigla dos estados brasileiros.
    * Lista de nomes mais comuns por UF: Onde faz uma requisição via APi em https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=UF e retorna uma lista de nomes mais comuns em cada estado, mostrando a frequencia e o ranking de cada nome.
    

### Gems:
  Nesse projeto as estão incluidas as gems:
    * gem 'activesupport'
    * gem 'faraday'
    * gem 'rake'
    * gem 'rspec'
    * gem 'simplecov', require: false, group: :test

### Testes:
  Para rodar os testes, no diretório escolhido, execute o comando:
    * rspec
    