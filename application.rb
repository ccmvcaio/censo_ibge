require 'faraday'
require 'json'
require_relative './app/models/state.rb'
require_relative './app/models/name.rb'

puts State.formatted_state_list

puts '=============================='
puts 'Digite a sigla da UF desejada:'
state_initials = gets.chomp
state_id = State.state_id_by_initials(state_initials)

puts 'Geral:'
puts Name.formatted_name_list(Name.general_ranked_list(state_id))
puts '==============================
Feminino:'
puts Name.formatted_name_list(Name.female_ranked_list(state_id))
puts '==============================
Masculino:'
puts Name.formatted_name_list(Name.male_ranked_list(state_id))
