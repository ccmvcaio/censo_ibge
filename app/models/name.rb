require_relative './base.rb'

class Name < Base
  attr_reader :name, :ranking, :frequency

  def initialize(name:, ranking:, frequency:)
    @name = name
    @ranking = ranking
    @frequency = frequency
  end

  def self.formatted_name_list(list)
    puts 'Nome - Frequencia - Ranking'
      list.map { |ranked_name| "#{ranked_name.name} - #{ranked_name.frequency} - #{ranked_name.ranking}"}
  end

  private

  def self.general_ranked_list(state_id)
    response = self.response_url(state_id, '')
    check_status_to_create_name(response)
  end

  def self.female_ranked_list(state_id)
    response = self.response_url(state_id, '&sexo=F')
    check_status_to_create_name(response)
  end

  def self.male_ranked_list(state_id)
    response = self.response_url(state_id, '&sexo=M')
    check_status_to_create_name(response)
  end

  def self.response_url(state_id, genre_query)
    Faraday.get("https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=#{state_id}#{genre_query}")
  end
end