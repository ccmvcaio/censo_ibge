class State
  attr_reader :state_id, :initials, :name

  def initialize(state_id:, initials:, name:)
    @state_id = state_id
    @initials = initials
    @name = name
  end
  
  def self.formatted_state_list
    self.state_list.map { |state| "#{state.name} - #{state.initials}" }
  end

  private
  
  def self.state_list
    response = Faraday.get('http://servicodados.ibge.gov.br/api/v1/localidades/estados')
    return {} unless response.status == 200

    state_list_json = JSON.parse(response.body, symbolize_names: true)
    state_list_json.map { |state| new(state_id: state[:id], initials: state[:sigla],
                                      name: state[:nome])}
  end

  def self.state_id_by_initials(initials)
    state_id = ''
    self.state_list.each { |state| state_id = state.state_id.to_s if initials.upcase == state.initials.upcase }
    state_id
  end
end
