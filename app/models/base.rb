require_relative './name.rb'

class Base
  def self.check_status_to_create_name(response)
    return {} unless response.status == 200
    
    list = JSON.parse(response.body, symbolize_names: true)
    list[0][:res].map { |ranked_name| new(name: ranked_name[:nome],
                                          ranking: ranked_name[:ranking],
                                          frequency: ranked_name[:frequencia]) }
  end
end
