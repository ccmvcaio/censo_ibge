FROM ruby:2.7.1

RUN apt-get update
ENV INSTALL_PATH /censo

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN gem install bundler:2.1.4
RUN bundle install

COPY . $INSTALL_PATH